package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	// Test for alpha and number
	@Test
	public void testIsValidLoginAlphaAndNumberRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nghao12345" ) );
	}
	
	@Test
	public void testIsValidLoginAlphaAndNumberException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "!@#$" ) );
	}
	
	@Test
	public void testIsValidLoginAlphaAndNumberBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nghao1" ) );
	}
	
	@Test
	public void testIsValidLoginAlphaAndNumberBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "2nghao" ) );
	}
	
	
	// Test for number of character
	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ng123hao" ) );
	}
	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "!@#$" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nghao2" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "nghao" ) );
	}

}
