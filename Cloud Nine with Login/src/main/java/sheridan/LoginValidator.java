package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {
	
	private LoginValidator() {
		
	}

	public static boolean isValidLoginName( String loginName ) {	
		// Check if login name must have alpha characters and numbers only, (no special characters) but it should not start with a number
		if (!isAlphaOrNumberOnly(loginName)) {
			return false;
		}
		
		// Check if Login name must have at least 6 alphanumeric characters
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9]{6,}$");
		Matcher matcher = pattern.matcher(loginName);	
		
		return matcher.find();
	}
	
	private static boolean isAlphaOrNumberOnly(String loginName) {
		Pattern pattern = Pattern.compile("^[a-zA-Z]+[a-zA-Z0-9]*$");
		Matcher matcher = pattern.matcher(loginName);	
		
		return matcher.find();
	}
}
